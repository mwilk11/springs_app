# spring_app

An html form with JavaScript data validation for Hooke's Law. This is still a work in progress! The idea is to develop an interactive application to be used by physics students to visualize the effects of changes in the parameters of a spring system.